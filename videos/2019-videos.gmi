```
     __ __       _______ _______ _____ _______ 
 .--|  |  |_    |       |   _   | _   |   _   |
 |  _  |   _|   |___|   |.  |   |.|   |   |   |
 |_____|____|    /  ___/|.  |   `-|.  |\___   |
                |:  1  \|:  1   | |:  |:  1   |
                |::.. . |::.. . | |::.|::.. . |
                `-------`-------' `---`-------'

```

# DistroTube Videos from 2019 (sorted in reverse order)

TITLE: Chat With Patrons (December 29, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-december-29-2019:e

TITLE: A Look Back At  2019 And Predicting 2020
=> https://odysee.com/@DistroTube:2/a-look-back-at-2019-and-predicting-2020:9

TITLE: Qtile - Configuring Your Layouts
=> https://odysee.com/@DistroTube:2/qtile-configuring-your-layouts:e

TITLE: Debian Debates Systemd Alternatives
=> https://odysee.com/@DistroTube:2/debian-debates-systemd-alternatives:6

TITLE: My Opinions Are Worthless
=> https://odysee.com/@DistroTube:2/my-opinions-are-worthless:6

TITLE: 'Tis The Season To Give - DT LIVE
=> https://odysee.com/@DistroTube:2/tis-the-season-to-give-dt-live:6

TITLE: Doom Emacs - Getting Started
=> https://odysee.com/@DistroTube:2/doom-emacs-getting-started:4

TITLE: Qtile - Getting Started And Setting Workspaces
=> https://odysee.com/@DistroTube:2/qtile-getting-started-and-setting:b

TITLE: Espanso Is A Cross Platform Text Expander
=> https://odysee.com/@DistroTube:2/espanso-is-a-cross-platform-text:a

TITLE: Elementary OS 5.1 "Hera" - Imaginative and Visionary
=> https://odysee.com/@DistroTube:2/elementary-os-5-1-hera-imaginative-and:d

TITLE: Playing The Trombone For The First Time In Months
=> https://odysee.com/@DistroTube:2/playing-the-trombone-for-the-first-time:3

TITLE: Affordable XLR Mic For Streamers
=> https://odysee.com/@DistroTube:2/affordable-xlr-mic-for-streamers:4

TITLE: The Basics of Emacs as a Text Editor
=> https://odysee.com/@DistroTube:2/the-basics-of-emacs-as-a-text-editor:a

TITLE: Why Use CAT Or GREP When You Can AWK?
=> https://odysee.com/@DistroTube:2/why-use-cat-or-grep-when-you-can-awk:a

TITLE: A Simple File Manager Using Dmenu
=> https://odysee.com/@DistroTube:2/a-simple-file-manager-using-dmenu:3

TITLE: Looking For Feedback On Ways To Improve My Channel
=> https://odysee.com/@DistroTube:2/looking-for-feedback-on-ways-to-improve:1

TITLE: Is Arch The New Ubuntu?
=> https://odysee.com/@DistroTube:2/is-arch-the-new-ubuntu:8

TITLE: Netris Is An Online Multiplayer Tetris Clone
=> https://odysee.com/@DistroTube:2/netris-is-an-online-multiplayer-tetris:d

TITLE: Quick Look At The NomadBSD Live System
=> https://odysee.com/@DistroTube:2/quick-look-at-the-nomadbsd-live-system:b

TITLE: Stumpwm Is One Strange Window Manager
=> https://odysee.com/@DistroTube:2/stumpwm-is-one-strange-window-manager:0

TITLE: Clean Up Your Audio With Audacity
=> https://odysee.com/@DistroTube:2/clean-up-your-audio-with-audacity:e

TITLE: The Scariest Film Of All Time
=> https://odysee.com/@DistroTube:2/the-scariest-film-of-all-time:c

TITLE: A Linuxy Live Stream
=> https://odysee.com/@DistroTube:2/a-linuxy-live-stream:c

TITLE: Cancel Culture and Safe Spaces Destroying Linux
=> https://odysee.com/@DistroTube:2/cancel-culture-and-safe-spaces:b

TITLE: Transparency With The Compton Compositor
=> https://odysee.com/@DistroTube:2/transparency-with-the-compton-compositor:d

TITLE: Completing Our Bash Script - More With Variables, Arrays And If-Then Statements
=> https://odysee.com/@DistroTube:2/completing-our-bash-script-more-with:f

TITLE: Thousands Flock To See Trump
=> https://odysee.com/@DistroTube:2/thousands-flock-to-see-trump:e

TITLE: Listen To Podcasts In The Terminal With Castero
=> https://odysee.com/@DistroTube:2/listen-to-podcasts-in-the-terminal-with:d

TITLE: Does Linux Marketing Really Suck?
=> https://odysee.com/@DistroTube:2/does-linux-marketing-really-suck:2

TITLE: Shocking Footage Caught On Camera
=> https://odysee.com/@DistroTube:2/shocking-footage-caught-on-camera:e

TITLE: Windows 7 Users Migrate To Windows 10 Rather Than Linux
=> https://odysee.com/@DistroTube:2/windows-7-users-migrate-to-windows-10:5

TITLE: The Age of the Introvert
=> https://odysee.com/@DistroTube:2/the-age-of-the-introvert:6

TITLE: Arch Linux Is The Ideal Beginner's Distro
=> https://odysee.com/@DistroTube:2/arch-linux-is-the-ideal-beginner-s:c

TITLE: Chat With Patrons (October 30, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-october-30-2019:4

TITLE: An Introduction to DistroTube
=> https://odysee.com/@DistroTube:2/an-introduction-to-distrotube:3

TITLE: Today's Terminology - Blue Pilled or Red Pilled
=> https://odysee.com/@DistroTube:2/today-s-terminology-blue-pilled-or-red:c

TITLE: Bash Scripting - Variables, Arrays, If-Then-Else
=> https://odysee.com/@DistroTube:2/bash-scripting-variables-arrays-if-then:2

TITLE: Linux Elitists Are Frauds - Making Their Linux Life Harder To Look Cool
=> https://odysee.com/@DistroTube:2/linux-elitists-are-frauds-making-their:c

TITLE: Monday is Fun Day (Part Two) - DT Live
=> https://odysee.com/@DistroTube:2/monday-is-fun-day-part-two-dt-live:0

TITLE: Monday is Fun Day (Part One) - DT Live
=> https://odysee.com/@DistroTube:2/monday-is-fun-day-part-one-dt-live:6

TITLE: Ubuntu 19.10 "Eoan Ermine" - Installation and First Look
=> https://odysee.com/@DistroTube:2/ubuntu-19-10-eoan-ermine-installation:d

TITLE: Hacking on Xmonad - GridSelect, ToggleStruts, ToggleBorders
=> https://odysee.com/@DistroTube:2/hacking-on-xmonad-gridselect:a

TITLE: Manjaro Awesome Edition
=> https://odysee.com/@DistroTube:2/manjaro-awesome-edition:9

TITLE: Playing With Emacs, Xmonad and Xmobar - DT Live
=> https://odysee.com/@DistroTube:2/playing-with-emacs-xmonad-and-xmobar-dt:c

TITLE: Switching to GNU Emacs
=> https://odysee.com/@DistroTube:2/switching-to-gnu-emacs:a

TITLE: Nu Shell - A Modern Shell For Today's User
=> https://odysee.com/@DistroTube:2/nu-shell-a-modern-shell-for-today-s-user:e

TITLE: Neofetch is BLOAT! Try pfetch instead.
=> https://odysee.com/@DistroTube:2/neofetch-is-bloat-try-pfetch-instead:a

TITLE: A Comprehensive Guide To Tiling Window Managers
=> https://odysee.com/@DistroTube:2/a-comprehensive-guide-to-tiling-window:b

TITLE: Taking Into Account, Ep. 50 - Windows 11, Ubuntu 32-bit, Cryptojacking, Pi 4, Trash Computers
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-50-windows-11:9

TITLE: The Future Of Free Software Without Richard Stallman
=> https://odysee.com/@DistroTube:2/the-future-of-free-software-without:9

TITLE: Media Demands Richard Stallman Be Fired
=> https://odysee.com/@DistroTube:2/media-demands-richard-stallman-be-fired:5

TITLE: My Workflow - Tiling Window Managers, Workspaces and Multi-Monitors
=> https://odysee.com/@DistroTube:2/my-workflow-tiling-window-managers:d

TITLE: Drauger OS Installation and First Look
=> https://odysee.com/@DistroTube:2/drauger-os-installation-and-first-look:e

TITLE: Condres OS 19.09 Installation and First Look
=> https://odysee.com/@DistroTube:2/condres-os-19-09-installation-and-first:4

TITLE: Are Richard Stallman and Linus Torvalds Good for Linux?
=> https://odysee.com/@DistroTube:2/are-richard-stallman-and-linus-torvalds:6

TITLE: Chat With Patrons (August 30 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-august-30-2019:b

TITLE: Off Topic - Getting To The Gym
=> https://odysee.com/@DistroTube:2/off-topic-getting-to-the-gym:e

TITLE: Reddio - Reddit From The Command Line
=> https://odysee.com/@DistroTube:2/reddio-reddit-from-the-command-line:4

TITLE: Saving Time At The Command Line
=> https://odysee.com/@DistroTube:2/saving-time-at-the-command-line:9

TITLE: Command Line Audio Visualizer
=> https://odysee.com/@DistroTube:2/command-line-audio-visualizer:6

TITLE: New Audio Equipment - Rack, Preamp, Compressor, EQ, Mixer
=> https://odysee.com/@DistroTube:2/new-audio-equipment-rack-preamp:d

TITLE: The One Year Anniversary of the Death of Terry Davis
=> https://odysee.com/@DistroTube:2/the-one-year-anniversary-of-the-death-of:e

TITLE: Switching To Bspwm - Initial Thoughts
=> https://odysee.com/@DistroTube:2/switching-to-bspwm-initial-thoughts:0

TITLE: Endeavour OS - Installation and First Look
=> https://odysee.com/@DistroTube:2/endeavour-os-installation-and-first-look:5

TITLE: Manjaro Ditches LibreOffice For Proprietary Garbage
=> https://odysee.com/@DistroTube:2/manjaro-ditches-libreoffice-for:e

TITLE: Chat With Patrons (July 31, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-july-31-2019:7

TITLE: After 500 Videos Made, A Time For Reflection
=> https://odysee.com/@DistroTube:2/after-500-videos-made-a-time-for:6

TITLE: The Fast And Flexible Albert Launcher
=> https://odysee.com/@DistroTube:2/the-fast-and-flexible-albert-launcher:8

TITLE: Taking Into Account, Ep. 47 - Endeavour, Alternative OSes, VLC Flaw, Dropbox, Deepin, Me and GNOME
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-47-endeavour:6

TITLE: Linux Youtubers That You Should Be Watching
=> https://odysee.com/@DistroTube:2/linux-youtubers-that-you-should-be:9

TITLE: Is GNOME As Garbage As I Initially Thought?
=> https://odysee.com/@DistroTube:2/is-gnome-as-garbage-as-i-initially:9

TITLE: Vim Macros Make The Impossible Possible
=> https://odysee.com/@DistroTube:2/vim-macros-make-the-impossible-possible:4

TITLE: Command Line App For Downloading YouTube Videos
=> https://odysee.com/@DistroTube:2/command-line-app-for-downloading-youtube:b

TITLE: Debian vs Arch. Which Is The Best Distro?
=> https://odysee.com/@DistroTube:2/debian-vs-arch-which-is-the-best-distro:8

TITLE: Material Shell - Turn GNOME Into A Tiling Window Manager
=> https://odysee.com/@DistroTube:2/material-shell-turn-gnome-into-a-tiling:f

TITLE: Arch Linux Installation Guide (2019)
=> https://odysee.com/@DistroTube:2/arch-linux-installation-guide-2019:3

TITLE: Hexagon OS 1.0 - Anything Worth Seeing Here?
=> https://odysee.com/@DistroTube:2/hexagon-os-1-0-anything-worth-seeing:e

TITLE: Q4OS Linux 3.7 Testing
=> https://odysee.com/@DistroTube:2/q4os-linux-3-7-testing:0

TITLE: Would Linux Users Pay For Microsoft Or Adobe Software?
=> https://odysee.com/@DistroTube:2/would-linux-users-pay-for-microsoft-or:6

TITLE: Taking Into Account, Ep. 46 (LIVE)
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-46-live:6

TITLE: Regolith Linux - Combining Ubuntu With i3
=> https://odysee.com/@DistroTube:2/regolith-linux-combining-ubuntu-with-i3:f

TITLE: Web Apps That Aren't Bloated! Using Curl.
=> https://odysee.com/@DistroTube:2/web-apps-that-aren-t-bloated-using-curl:a

TITLE: Free Software or Open Source Software? Is There A Difference?
=> https://odysee.com/@DistroTube:2/free-software-or-open-source-software-is:3

TITLE: Why Do I Do This?
=> https://odysee.com/@DistroTube:2/why-do-i-do-this:b

TITLE: Unimatrix Is The Better Matrix
=> https://odysee.com/@DistroTube:2/unimatrix-is-the-better-matrix:8

TITLE: The Linux Community Is Not Your "Safe Space"
=> https://odysee.com/@DistroTube:2/the-linux-community-is-not-your-safe:2

TITLE: Taking Into Account, Ep 44 - Angry Devs, Quake, Snap Store, Linux Mint Should Die
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-44-angry-devs:3

TITLE: Pass - The Standard Unix Password Manager
=> https://odysee.com/@DistroTube:2/pass-the-standard-unix-password-manager:0

TITLE: Distro Roundup (May 27, 2019) - TAILS, Kali, OpenSUSE, BlackArch
=> https://odysee.com/@DistroTube:2/distro-roundup-may-27-2019-tails-kali:b

TITLE: The Free Software Song - DT and RMS Duet
=> https://odysee.com/@DistroTube:2/the-free-software-song-dt-and-rms-duet:0

TITLE: Chat With Patrons (May 26, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-may-26-2019:6

TITLE: Dealing With Trolls On Social Media
=> https://odysee.com/@DistroTube:2/dealing-with-trolls-on-social-media:5

TITLE: Taking Into Account, Ep. 43 - Huawei, South Korea, Firefox, GitHub Sponsors, Manjaro, Antergos
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-43-huawei-south:0

TITLE: After Two Weeks With AwesomeWM...
=> https://odysee.com/@DistroTube:2/after-two-weeks-with-awesomewm:f

TITLE: BREAKING NEWS - Antergos Linux Project Ends - What Are Your Options?
=> https://odysee.com/@DistroTube:2/breaking-news-antergos-linux-project:3

TITLE: Distro Roundup (May 20, 2019) - OpenIndiana, OpenMandriva, ArcoLinux, Peppermint, RoboLinux
=> https://odysee.com/@DistroTube:2/distro-roundup-may-20-2019-openindiana:a

TITLE: The ErgoDox EZ - The Ridiculous, Incredible Mechanical Keyboard
=> https://odysee.com/@DistroTube:2/the-ergodox-ez-the-ridiculous-incredible:9

TITLE: Taking Into Account, Ep. 42 - BTW Arch WSL, Contributing to FOSS, Zombieload, Indian Schools, Bing
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-42-btw-arch-wsl:6

TITLE: What Is "Software Bloat"?
=> https://odysee.com/@DistroTube:2/what-is-software-bloat:0

TITLE: Systemd Haters Getting Their Panties In A Twist
=> https://odysee.com/@DistroTube:2/systemd-haters-getting-their-panties-in:a

TITLE: Why I Choose Free And Open Source Software
=> https://odysee.com/@DistroTube:2/why-i-choose-free-and-open-source:2

TITLE: Taking Into Account, Ep. 41 - Break Up Facebook, Linux in Windows, Eoan Ermine, MS + Red Hat, Tilix
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-41-break-up:e

TITLE: The Window Manager Carousel Continues - Awesome Day One
=> https://odysee.com/@DistroTube:2/the-window-manager-carousel-continues:b

TITLE: Accused Of Racism. I Quit My Job. Now What?
=> https://odysee.com/@DistroTube:2/accused-of-racism-i-quit-my-job-now-what:3

TITLE: Taking Into Account, Ep. 40 - New Red Hat, GNOME is a Mess, Librem One, Win 10 Bloated, Mozilla IRC
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-40-new-red-hat:4

TITLE: Changing Your Default Shell
=> https://odysee.com/@DistroTube:2/changing-your-default-shell:b

TITLE: Fedora 30 Workstation - Installation And First Impression
=> https://odysee.com/@DistroTube:2/fedora-30-workstation-installation-and:9

TITLE: Chat With Patrons (April 27, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-april-27-2019:0

TITLE: Taking Into Account, Ep. 38 - Windows 10, Open Source Wins, VSCodium, Pengwin, Davinci, Kdenlive
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-38-windows-10:1

TITLE: The Many Flavors of Ubuntu 19.04 "Disco Dingo"
=> https://odysee.com/@DistroTube:2/the-many-flavors-of-ubuntu-19-04-disco:5

TITLE: An Overview of Snap Packages
=> https://odysee.com/@DistroTube:2/an-overview-of-snap-packages:1

TITLE: The Web Is Broken Beyond Repair. The Alternative? GOPHER!
=> https://odysee.com/@DistroTube:2/the-web-is-broken-beyond-repair-the:3

TITLE: Taking Into Account, Ep. 37 - Linux Troubles, Mint Devs Unhappy, China Bans Crypto, MS Linux Apps
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-37-linux-troubles:6

TITLE: Command Line Shenanigans - Tomfoolery In The Terminal
=> https://odysee.com/@DistroTube:2/command-line-shenanigans-tomfoolery-in:a

TITLE: GNU Nano With Improved Syntax Highlighting
=> https://odysee.com/@DistroTube:2/gnu-nano-with-improved-syntax:3

TITLE: My New Computer - DT Joins Team Red - Threadripper & Radeon VII
=> https://odysee.com/@DistroTube:2/my-new-computer-dt-joins-team-red:5

TITLE: Taking Into Account, Ep. 36 - Linus Interview, Linux Gaming, VMWare, SPURV, Ubuntu, Fedora
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-36-linus:4

TITLE: Sabayon 19.03 GNOME - DT Live
=> https://odysee.com/@DistroTube:2/sabayon-19-03-gnome-dt-live:b

TITLE: Microsoft Edge Coming To Linux? Google Essentially Owns The Internet
=> https://odysee.com/@DistroTube:2/microsoft-edge-coming-to-linux-google:0

TITLE: Taking Into Account, Ep. 35 - Linux Foundation, Article 13, NexDock 2, Sway, GNOME
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-35-linux:8

TITLE: Big YouTubers And Linux - So Little Coverage And It's Usually Bad!
=> https://odysee.com/@DistroTube:2/big-youtubers-and-linux-so-little:9

TITLE: Chat With Patrons (March 24, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-march-24-2019:b

TITLE: Curseradio - Internet Radio For Your Terminal
=> https://odysee.com/@DistroTube:2/curseradio-internet-radio-for-your:1

TITLE: Taking Into Account, Ep. 34 - Stadia, Open Source Money, Windows 7, Stallman, App Releases, IRC
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-34-stadia-open:f

TITLE: Vim Makes Everything Better, Especially Your File Manager And Shell!
=> https://odysee.com/@DistroTube:2/vim-makes-everything-better-especially:8

TITLE: Solus 4.0 'Fortitude" - The Budgie Edition - Installation and First Look
=> https://odysee.com/@DistroTube:2/solus-4-0-fortitude-the-budgie-edition:0

TITLE: State of the Channel - DT Live
=> https://odysee.com/@DistroTube:2/state-of-the-channel-dt-live:2

TITLE: Window Manager Hopping Herbstluftwm
=> https://odysee.com/@DistroTube:2/window-manager-hopping-herbstluftwm:a

TITLE: Taking Into Account, Ep. 33 - GNOME, Skype, OpenJS, PureOS, Firefox Send, Poll Results
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-33-gnome-skype:1

TITLE: Trayer - A Lightweight System Tray For Your Linux Desktop
=> https://odysee.com/@DistroTube:2/trayer-a-lightweight-system-tray-for:5

TITLE: 4MLinux 28.0 - Installation and First Look
=> https://odysee.com/@DistroTube:2/4mlinux-28-0-installation-and-first-look:7

TITLE: A Command Line Pastebin and a Terminal Session Recorder - Termbin & Asciinema
=> https://odysee.com/@DistroTube:2/a-command-line-pastebin-and-a-terminal:c

TITLE: Taking Into Account, Ep. 32 - Intel CPUs, Windows Calculator, Ubuntu Studio, Mint, Good vs Evil
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-32-intel-cpus:8

TITLE: LSD on Linux - the next gen "ls" command
=> https://odysee.com/@DistroTube:2/lsd-on-linux-the-next-gen-ls-command:5

TITLE: Xmonad and Named Scratchpads
=> https://odysee.com/@DistroTube:2/xmonad-and-named-scratchpads:e

TITLE: Vifm and Überzug (Ueberzug) Image Previews
=> https://odysee.com/@DistroTube:2/vifm-and-berzug-ueberzug-image-previews:1

TITLE: Taking Into Account, Ep. 31 - Ubuntu Drops APT, Ransomware, Redis, Social Media Crime, Icons
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-31-ubuntu-drops:d

TITLE: Zipping Your Files In Linux
=> https://odysee.com/@DistroTube:2/zipping-your-files-in-linux:f

TITLE: Newsboat RSS Reader - Not Just For News Feeds
=> https://odysee.com/@DistroTube:2/newsboat-rss-reader-not-just-for-news:1

TITLE: Taking Into Account, Ep. 30 - Linux Life Support, Snap Store, WSL, Chrome OS, Raspberry Pi
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-30-linux-life:9

TITLE: Movie Monad - Lightweight And Cross Platform Video Player
=> https://odysee.com/@DistroTube:2/movie-monad-lightweight-and-cross:3

TITLE: The Surf Browser - Bookmarks, Stylesheets and Tabs
=> https://odysee.com/@DistroTube:2/the-surf-browser-bookmarks-stylesheets:0

TITLE: Switching to Xmonad - Not Exactly Day One
=> https://odysee.com/@DistroTube:2/switching-to-xmonad-not-exactly-day-one:f

TITLE: Taking Into Account, Ep. 29 - Linux gaming, Taking Code Back, Microsoft OpenChain, Snapd, Void Linux
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-29-linux-gaming:b

TITLE: Time To Leave Dwm Behind - My Thoughts on Suckless
=> https://odysee.com/@DistroTube:2/time-to-leave-dwm-behind-my-thoughts-on:d

TITLE: Redcore Linux 1812 LXQt - Installation and First Look
=> https://odysee.com/@DistroTube:2/redcore-linux-1812-lxqt-installation-and:3

TITLE: Taking Into Account, Ep. 28 - Open Source, GNOME panel, SpeakUp, Shellbot, New User Distros, Apps
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-28-open-source:3

TITLE: New Suckless Releases - dwm 6.2 and dmenu 4.9
=> https://odysee.com/@DistroTube:2/new-suckless-releases-dwm-6-2-and-dmenu:6

TITLE: The Vim Tutorial - Part Two - More Commands
=> https://odysee.com/@DistroTube:2/the-vim-tutorial-part-two-more-commands:e

TITLE: Dotfiles Are Everywhere. We've Lost Control Of Our Home Directories!
=> https://odysee.com/@DistroTube:2/dotfiles-are-everywhere-we-ve-lost:4

TITLE: Day 12 (dwm) -  Status Bar Configuration
=> https://odysee.com/@DistroTube:2/day-12-dwm-status-bar-configuration:4

TITLE: Taking Into Account, Ep. 27 - New Pi, Olive, Facebook, 4K Chromebook, App Releases
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-27-new-pi-olive:2

TITLE: Mageia Dropped From DistroWatch "Major Distros" Page. What Should Replace It?
=> https://odysee.com/@DistroTube:2/mageia-dropped-from-distrowatch-major:a

TITLE: Day 7 - dwm and suckless - I Hate The Patching!
=> https://odysee.com/@DistroTube:2/day-7-dwm-and-suckless-i-hate-the:d

TITLE: Taking Into Account, Ep. 26 - Apt Bug, Ubuntu Core, Chromebooks, SUSE on ARM, MS phones
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-26-apt-bug-ubuntu:8

TITLE: Three Weeks With i3 - Time For A New Challenge - SUCKLESS!
=> https://odysee.com/@DistroTube:2/three-weeks-with-i3-time-for-a-new:d

TITLE: ArcoLinux Xmonad - A Quick First Look
=> https://odysee.com/@DistroTube:2/arcolinux-xmonad-a-quick-first-look:8

TITLE: Taking Into Account, Ep. 25 - Systemd, VLC, MongoDB, SuperTuxKart, FSF
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-25-systemd-vlc:e

TITLE: Git Bare Repository - A Better Way To Manage Dotfiles
=> https://odysee.com/@DistroTube:2/git-bare-repository-a-better-way-to:7

TITLE: Icons And Image Previews In Vifm, Plus Xterm Is Great!
=> https://odysee.com/@DistroTube:2/icons-and-image-previews-in-vifm-plus:a

TITLE: Free And Open Chat Sunday (January 13, 2019)
=> https://odysee.com/@DistroTube:2/free-and-open-chat-sunday-january-13:8

TITLE: Switching to i3 - Day 12 - Binding Modes
=> https://odysee.com/@DistroTube:2/switching-to-i3-day-12-binding-modes:a

TITLE: Noob Questions and Comments - ANSWERED!
=> https://odysee.com/@DistroTube:2/noob-questions-and-comments-answered:1

TITLE: Taking Into Account, Ep. 24 - Windows Reserve Storage, Game Devs, Snap Games, GDPR, Ubuntu $, FLOSS
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-24-windows:6

TITLE: Three Manpage Alternatives - bropages, cheat and tldr
=> https://odysee.com/@DistroTube:2/three-manpage-alternatives-bropages:d

TITLE: Why Use A Tiling Window Manager?  Speed, Efficiency and Customization!
=> https://odysee.com/@DistroTube:2/why-use-a-tiling-window-manager-speed:4

TITLE: Chat With Patrons (January 6, 2019)
=> https://odysee.com/@DistroTube:2/chat-with-patrons-january-6-2019:a

TITLE: Odio - Radio Streaming App for Windows, Mac and Linux
=> https://odysee.com/@DistroTube:2/odio-radio-streaming-app-for-windows-mac:0

TITLE: Switching to i3 - Day Four
=> https://odysee.com/@DistroTube:2/switching-to-i3-day-four:b

TITLE: Taking Into Account, Ep. 23 - Firefox ads, Snaps, Kernel commits, Bloodstained, Chromebooks, i3
=> https://odysee.com/@DistroTube:2/taking-into-account-ep-23-firefox-ads:6

TITLE: Switching to i3 - Day One
=> https://odysee.com/@DistroTube:2/switching-to-i3-day-one:9

TITLE: eDEX-UI - Fullscreen Terminal Inspired By Hollywood
=> https://odysee.com/@DistroTube:2/edex-ui-fullscreen-terminal-inspired-by:3

## Video Categories

=> ../videos/2017-videos.gmi Videos from 2017
=> ../videos/2018-videos.gmi Videos from 2018
=> ../videos/2019-videos.gmi Videos from 2019
=> ../videos/2020-videos.gmi Videos from 2020
=> ../videos/2021-videos.gmi Videos from 2021
